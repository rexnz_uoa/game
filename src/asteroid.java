import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class asteroid {
    private Image ast2, ast1;
    private static final int WIDTH = 100, HEIGHT = 100;
    private int x, y, size, speed, width, height;

    public asteroid(int x, int y, int speed, int size) {
        try {
            this.ast1 = ImageIO.read(new File("asteroidSmall.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.x = x;
        this.y = y;
        this.speed = speed;
        this.size = size;
        this.width = WIDTH;
        this.height = HEIGHT;
    }

    public static int getWIDTH() {
        return WIDTH;
    }

    public static int getHEIGHT() {
        return HEIGHT;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getSize() {
        return size;
    }

    public int getSpeed() {
        return speed;
    }

    public void move(int windowWidth, int windowHeight) {
        y = y + speed;
        if (y > windowHeight){
            y = -100;


        }

    }

    public void drawAsteroid(Graphics g, int sx1, int sy1) {
        g.drawImage(ast2, x, y, x + width, y + height, sx1, sy1, width + sx1, height + sy1, null);
        g.drawImage(ast1, x, y, x + width, y + height, sx1, sy1, width + sx1, height + sy1, null);
    }
}
