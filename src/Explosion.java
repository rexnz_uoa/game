import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Explosion {

    private static final int SPRITE_WIDTH = 128, SPRITE_HEIGHT = 128;

    private int x, y, width, height;

    private Direction direction;

    private Image image;

    public boolean explosionEnabled = false;


    public Explosion(int x, int y) {

        direction = Direction.Up;

        try {
            this.image = ImageIO.read(new File("game-over.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.x = x;
        this.y = y;
        this.width = SPRITE_WIDTH;
        this.height = SPRITE_HEIGHT;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void paint(Graphics g) {

        drawExplosion(g);

    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void drawExplosion(Graphics g) {


//        new delayAnimation("th", g);
//        g.drawImage(image, x, y, x + width, y + height, 128, 128, width + 128, height + 128, null);


//        try {
//            TimeUnit.MILLISECONDS.sleep(20);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        g.drawImage(image, x, y, x + width, y + height, 0, 0, 0, 0, null);

        if (explosionEnabled)

            g.drawImage(image, x, y, null);


        if (explosionEnabled)
            for (int i = 0; i < 896; i += 128)
                for (int j = 0; j < 896; j += 128) {
//                    g.drawImage(image, x, y, x + width, y + height, j, i, width + j, height + i, null);
//
                }

    }


//    private void drawExplosion(Graphics g, int sx1, int sy1) {
//        g.drawImage(image, x, y, x + width, y + height, sx1, sy1, width + sx1, height + sy1, null);
//    }

    public class delayAnimation implements Runnable {

        String name;
        Thread thrd;
        Graphics g;

        public delayAnimation(String name, Graphics g) {
            this.name = name;
            thrd = new Thread(this, name);
            this.g = g;
            thrd.start();
        }

        @Override
        public void run() {

            if (explosionEnabled)
                for (int i = 0; i < 896; i += 128)
                    for (int j = 0; j < 896; j += 128) {
                        try {
                            thrd.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        g.drawImage(image, x, y, x + width, y + height, j, i, width + j, height + i, null);

                    }


        }
    }

}
