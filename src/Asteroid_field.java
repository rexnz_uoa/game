import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Asteroid_field {

    private List<asteroid> asteroids;
    Random rnd;

    public Asteroid_field(int width, int height) {

        asteroids = new ArrayList<>();
        rnd = new Random();

        for (int i = 0; i < 8; i++) {
            int rndHeight = rnd.nextInt(height);
            int rndWidth = rnd.nextInt(width);
            int rndSize = rnd.nextInt(4) + 3; //between 2 and 6 (inc)
            int rndSpeed = rnd.nextInt(5) + 4; //between 5 and 11 (inc)
            asteroids.add(new asteroid(rndHeight, rndWidth, rndSpeed, rndSize));

        }

    }


    public List<asteroid> getAsteroids() {
        return asteroids;
    }

    public void move(int windowWidth, int windowHeight) {

        for (int i = 0; i < asteroids.size(); i++) {
            asteroids.get(i).move(windowWidth, windowHeight);


        }
    }

    public void paint(Graphics g) {

        for (int i = 0; i < asteroids.size(); i++) {
            asteroids.get(i).drawAsteroid(g,0,0);
        }
    }




}





