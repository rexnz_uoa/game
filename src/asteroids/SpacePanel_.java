//import javax.swing.*;
//import java.awt.*;
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//import java.awt.event.KeyEvent;
//import java.awt.event.KeyListener;
//
//import static java.awt.event.KeyEvent.*;
//
///**
// * Created by Andrew Meads on 9/01/2018.
// * <p>
// * TODO Steps 4 and 5 (implement interfaces)
// */
//public class SpacePanel_ extends JPanel implements ActionListener, KeyListener {
//
//    public static final int PREFERRED_WIDTH = 600;
//    public static final int PREFERRED_HEIGHT = 500;
//
//    private Starfield stars;
//
//    private Asteroid_field asteroids;
//
//    private Spaceship ship;
//
//    private Direction moveDirection = Direction.None;
//
//    private Timer timer;
//
//
//    public SpacePanel_() {
//
//        timer = new Timer(20, this);
//
//        addKeyListener(this);
//
//
//        setPreferredSize(new Dimension(PREFERRED_WIDTH, PREFERRED_HEIGHT));
//
//        this.stars = new Starfield(PREFERRED_WIDTH, PREFERRED_HEIGHT);
//
//        this.ship = new Spaceship(PREFERRED_WIDTH / 2, PREFERRED_HEIGHT / 2, 10);
//
//        this.asteroids = new Asteroid_field(PREFERRED_WIDTH , PREFERRED_HEIGHT);
//
//    }
//
//    public void start() {
//
//        this.timer.start();
//
//
//    }
//
//    public void stop() {
//        this.timer.stop();
//    }
//
//    @Override
//    protected void paintComponent(Graphics g) {
//        super.paintComponent(g);
//
//        g.setColor(Color.black);
//        g.fillRect(0, 0, getWidth(), getHeight());
//
//        stars.paint(g);
//        ship.paint(g);
//        asteroids.paint(g);
//
//
//    }
//
//    @Override
//    public void actionPerformed(ActionEvent e) {
//
//        stars.move(getWidth(), getHeight());
//        ship.move(moveDirection, getWidth(), getHeight());
//        asteroids.move(getWidth(),getHeight());
//
//
//
//        requestFocusInWindow();
//        repaint();
//
//    }
//
//    @Override
//    public void keyTyped(KeyEvent e) {
//
//    }
//
//    @Override
//    public void keyPressed(KeyEvent e) {
//        System.err.println("code key:" + e.getKeyCode());
//
//
//        moveDirection = Direction.None;
//
//        switch (e.getKeyCode()) {
//
//            case VK_UP:
//                moveDirection = Direction.Up;
//                break;
//
//            case VK_DOWN:
//                moveDirection = Direction.Down;
//                break;
//
//            case VK_LEFT:
//                moveDirection = Direction.Left;
//                break;
//
//            case VK_RIGHT:
//                moveDirection = Direction.Right;
//                break;
//
//        }
//    }
//
//    @Override
//    public void keyReleased(KeyEvent e) {
//
//        switch (e.getKeyCode()) {
//
//            case VK_UP:
//                moveDirection = Direction.Up;
//                break;
//
//            case VK_DOWN:
//                moveDirection = Direction.Down;
//                break;
//
//            case VK_LEFT:
//                moveDirection = Direction.Left;
//                break;
//
//            case VK_RIGHT:
//                moveDirection = Direction.Right;
//                break;
//
//        }
//
//    }
//}