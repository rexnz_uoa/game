import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;

import static java.awt.event.KeyEvent.*;

/**
 * Created by Andrew Meads on 9/01/2018.
 * <p>
 * TODO Steps 4 and 5 (implement interfaces)
 */
public class SpacePanel extends JPanel implements ActionListener, KeyListener {

    Random random = new Random();


    public static final int PREFERRED_LIFE_amount = 1500;
    public static final int PREFERRED_WIDTH = 800;
    public static final int PREFERRED_HEIGHT = 800;

    private Asteroid_field asteroids;

    private Starfield stars;

    private Spaceship ship;

    private Explosion explosion;

    private ArrayList<Enemy> enemyList = new ArrayList<>();

    private Direction moveDirection = Direction.None;

    private Timer timer;

    private int life = PREFERRED_LIFE_amount;

    public SpacePanel() {

        timer = new Timer(20, this);
        addKeyListener(this);


        this.asteroids = new Asteroid_field(PREFERRED_WIDTH, PREFERRED_HEIGHT);


        setPreferredSize(new Dimension(PREFERRED_WIDTH, PREFERRED_HEIGHT));

        this.stars = new Starfield(PREFERRED_WIDTH, PREFERRED_HEIGHT);

        this.ship = new Spaceship(PREFERRED_WIDTH / 2, PREFERRED_HEIGHT - PREFERRED_HEIGHT / 8, 10);

        enemyList.add(new Enemy(PREFERRED_WIDTH / 2, PREFERRED_HEIGHT / 2, 3));
        enemyList.add(new Enemy(PREFERRED_WIDTH / 1, PREFERRED_HEIGHT / 4, 4));
        enemyList.add(new Enemy(PREFERRED_WIDTH / 4, PREFERRED_HEIGHT / 2, 2));
        enemyList.add(new Enemy(PREFERRED_WIDTH / 6, PREFERRED_HEIGHT / 2, 6));
        enemyList.add(new Enemy(PREFERRED_WIDTH / 2, PREFERRED_HEIGHT / 6, 1));
        enemyList.add(new Enemy(PREFERRED_WIDTH / 1, PREFERRED_HEIGHT / 8, 10));

        this.explosion = new Explosion(PREFERRED_WIDTH / 2, PREFERRED_HEIGHT / 2);
    }

    public void start() {

        this.timer.start();

    }

    public void stop() {
        this.timer.stop();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.setColor(Color.black);
        g.fillRect(0, 0, getWidth(), getHeight());

        stars.paint(g);
        ship.paint(g);
        for (Enemy enemy : enemyList) {
            enemy.paint(g);
        }
        explosion.paint(g);

        asteroids.paint(g);


    }

    @Override
    public void actionPerformed(ActionEvent e) {

        asteroids.move(getWidth(), getHeight());


        stars.move(getWidth(), getHeight());
        ship.move(moveDirection, getWidth(), getHeight());

        for (Enemy enemy : enemyList) {

            switch (random.nextInt(4)) {
                case 0:
                    enemy.move(Direction.Left, getWidth(), getHeight());
                    break;
                case 1:
                    enemy.move(Direction.Right, getWidth(), getHeight());
                    break;
                case 2:
                    enemy.move(Direction.Up, getWidth(), getHeight());
                    break;
                case 3:
                    enemy.move(Direction.Down, getWidth(), getHeight());
                    break;
            }
        }

        //check if spacecraft hits enemies

        for (Enemy enemy : enemyList) {

            //check collision
            if ((Math.abs(ship.getX() - enemy.getX()) < (ship.getWidth() / 2 + enemy.getWidth() / 2)) &&
                    (Math.abs(ship.getY() - enemy.getY()) < Math.abs(ship.getHeight() / 2 + enemy.getHeight() / 2))) {
                life -= 25;
                System.err.println("collision with enemy: " + enemy.toString() + " " + life);
                if (life < 0) {
                    explosion.setX(ship.getX());
                    explosion.setY(ship.getY());
                    explosion.explosionEnabled = true;
                }
            }
        }
        System.out.println("life1=" + life);


        ArrayList<asteroid> a = new ArrayList<>();
        a.addAll(asteroids.getAsteroids());

        for (int i = 0; i < a.size(); i++) {


            if ((Math.abs(ship.getX() - a.get(i).getX()) < (ship.getWidth() / 2 + a.get(i).getWIDTH() / 2)) &&
                    (Math.abs(ship.getY() - a.get(i).getY()) < Math.abs(ship.getHeight() / 2 + a.get(i).getHEIGHT() / 2))) {

                System.err.println("hits asteroid");

                life -= 5;
                if (life < 0) {
                    explosion.setX(ship.getX());
                    explosion.setY(ship.getY());
                    explosion.explosionEnabled = true;
                }
            }
        }
        System.out.println("life2=" + life);

        requestFocusInWindow();
        repaint();

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        System.err.println("code key:" + e.getKeyCode());


        moveDirection = Direction.None;

        switch (e.getKeyCode()) {

            case VK_UP:
                moveDirection = Direction.Up;
                break;

            case VK_DOWN:
                moveDirection = Direction.Down;
                break;

            case VK_LEFT:
                moveDirection = Direction.Left;
                break;

            case VK_RIGHT:
                moveDirection = Direction.Right;
                break;

        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

        switch (e.getKeyCode()) {

            case VK_UP:
                moveDirection = Direction.Up;
                break;

            case VK_DOWN:
                moveDirection = Direction.Down;
                break;

            case VK_LEFT:
                moveDirection = Direction.Left;
                break;

            case VK_RIGHT:
                moveDirection = Direction.Right;
                break;

        }

    }
}